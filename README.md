# Some typographical postcards

Fonts to work-up:

- Motter Tektura
- Eurostile
- ITC Avant Garde
- Zorn (Laura Csocsan, https://lauracsocsan.xyz/collections/zorn/products/zorn-webfont)
- Papyrus

## Draft

- Handel Gothic
- VAG rounded
- ITC Souvenir
- Van Dijk

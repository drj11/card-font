package main

import (
	"flag"
	"fmt"
)

func main() {
	s := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	w := 79
	flag.IntVar(&w, "w", 79, "width of output in runes")
	flag.Parse()
	ripl(w, s)
}

// Output a ripple using the string s.
// The width is w runes.
func ripl(w int, s string) {
	N := 200
	for i := 0; i < N; i++ {
		fmt.Println(line(w, s))
		s = s[1:] + s[0:1]
	}
}

func line(w int, s string) string {
	a := ""
	for len([]rune(a)) < w {
		a += s
	}
	return string([]rune(a)[:w])
}
